package pro.siper.adept.sample.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pro.siper.adept.sample.R
import pro.siper.adept.sample.view.fragment.SamplesMenuFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, SamplesMenuFragment())
                .commit()
    }
}
