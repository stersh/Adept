package pro.siper.adept.core

import android.os.Parcelable
import android.util.SparseArray
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class AdeptViewHolder(itemView: View, val viewType: Int) : RecyclerView.ViewHolder(itemView) {
    private val views = SparseArray<View>()
    internal var hasNestedRv = false

    @Suppress("UNCHECKED_CAST")
    fun <V : View> findView(id: Int): V {
        if (views[id] == null) {
            val view = itemView.findViewById<V>(id)
            if (!hasNestedRv && view is RecyclerView) {
                hasNestedRv = true
            }
            views.put(id, view)
        }
        return views[id] as V
    }

    fun getRvStates(): SparseArray<Parcelable> {
        return SparseArray<Parcelable>().also {
            views
                    .filter { it is RecyclerView }
                    .map { it as RecyclerView }
                    .forEachWithKey { item, key ->
                        val lm = item.layoutManager
                        if (lm is LinearLayoutManager) {
                            it.put(key, lm.onSaveInstanceState())
                        }
                    }
        }
    }

    fun setRvStates(states: SparseArray<Parcelable>) {
        states.forEachWithKey { item, key ->
            (findView<RecyclerView>(key).layoutManager as LinearLayoutManager)
                    .onRestoreInstanceState(item)
        }
    }
}