package pro.siper.adept.core.endless

interface EndlessScrollingCallback {
    fun onLoadMore(currentPage: Int, totalItemsCount: Int): Boolean
}