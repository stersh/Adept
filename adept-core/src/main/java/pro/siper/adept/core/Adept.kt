package pro.siper.adept.core

import android.os.Parcelable
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pro.siper.adept.core.diff.DefaultDiffUtilCallback
import pro.siper.adept.core.diff.DiffUtilCallback
import pro.siper.adept.core.diff.DiffUtilCallbackCreator
import pro.siper.adept.core.endless.EndlessScrollingCallback
import pro.siper.adept.core.endless.EndlessScrollingListener
import kotlin.reflect.KClass

class Adept : RecyclerView.Adapter<AdeptViewHolder>() {
    val renderers: MutableMap<KClass<*>, ItemViewRenderer<*, *>> = mutableMapOf()
    val layouts: MutableMap<KClass<*>, Int> = mutableMapOf()

    val dataset: MutableList<Any?> = mutableListOf()

    var customBinder: BinderSetter<*>? = null

    private val LOADING_VIEW = -11

    private val viewTypes: MutableList<KClass<out Any>> = mutableListOf()

    private var diffUtilCallback: DiffUtilCallback? = null
    val useDiffUtil
        get() = diffUtilCallback != null

    private var endlessScrollingListener: EndlessScrollingListener? = null
    val useEndlessScrolling
        get() = endlessScrollingListener != null

    private var loadingLayoutResId = -1
    private val useLoadingView
        get() = loadingLayoutResId != -1

    private val rvStates = SparseArray<SparseArray<Parcelable>>()
    private var saveNestedRvStates = false

    override fun getItemViewType(position: Int): Int {
        if (useLoadingView && position == itemCount - 1 && endlessScrollingListener!!.hasNextPage) {
            return LOADING_VIEW
        }
        val type = dataset[position]!!::class
        if (viewTypes.indexOf(type) == -1) {
            viewTypes.add(type)
        }
        return viewTypes.indexOf(type)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdeptViewHolder {
        if (viewType == LOADING_VIEW) {
            val view = LayoutInflater
                    .from(parent.context)
                    .inflate(loadingLayoutResId, parent, false)
            return AdeptViewHolder(view, viewType)
        }

        val layout = layouts[viewTypes[viewType]]
                ?: throw IllegalArgumentException("Layout for ${viewTypes[viewType]} is not registered")
        val view = LayoutInflater
                .from(parent.context)
                .inflate(layout, parent, false)
        return AdeptViewHolder(view, viewType)
    }

    override fun getItemCount(): Int {
        return if (useLoadingView && endlessScrollingListener!!.hasNextPage) {
            dataset.size + 1
        } else {
            dataset.size
        }
    }

    override fun onBindViewHolder(
            holder: AdeptViewHolder,
            position: Int,
            payloads: MutableList<Any>) {
        onBind(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: AdeptViewHolder, position: Int) {
        onBind(holder, position)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        if (useEndlessScrolling) {
            recyclerView.addOnScrollListener(endlessScrollingListener!!)
        }
    }

    override fun onViewRecycled(holder: AdeptViewHolder) {
        super.onViewRecycled(holder)
        if (saveNestedRvStates && holder.hasNestedRv) {
            rvStates.put(holder.adapterPosition, holder.getRvStates())
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        if (useEndlessScrolling) {
            recyclerView.removeOnScrollListener(endlessScrollingListener!!)
        }
    }

    private fun onBind(
            holder: AdeptViewHolder,
            position: Int,
            payloads: MutableList<Any> = mutableListOf()) {

        if (useLoadingView && holder.viewType == LOADING_VIEW) return

        val vType = viewTypes[holder.viewType]
        val renderer = renderers[vType]
                ?: throw IllegalArgumentException("There are no renderer for $vType")

        if (customBinder == null) {
            renderer.anyTypeRender(dataset[position], DefaultViewBinder(holder), payloads)
        } else {
            renderer.anyTypeRender(
                    dataset[position],
                    customBinder!!.getCustomViewBinder(holder) as ViewBinder,
                    payloads
            )
        }

        if (saveNestedRvStates && rvStates[position] != null) {
            holder.setRvStates(rvStates[position])
        }
    }

    fun updateDataset(dataset: List<Any?>) {
        if (useDiffUtil) {
            val diff = DiffUtilCallbackCreator(
                    this.dataset,
                    dataset,
                    diffUtilCallback!!
            )
            val diffResult = DiffUtil.calculateDiff(diff)
            this.dataset.clear()
            this.dataset.addAll(dataset)
            diffResult.dispatchUpdatesTo(this)
        } else {
            this.dataset.clear()
            this.dataset.addAll(dataset)
        }
    }

    inline fun <reified D> addRenderer(layoutId: Int, renderer: DefaultItemViewRenderer<D>): Adept {
        if (layouts.containsKey(D::class)) {
            throw IllegalArgumentException("Renderer for ${D::class} already registered")
        }
        layouts[D::class] = layoutId
        renderers[D::class] = renderer
        return this
    }

    inline fun <reified D> addRenderer(
            layoutId: Int,
            crossinline renderer: (data: D, viewBinder: DefaultViewBinder, payloads: MutableList<Any>) -> Unit): Adept {
        addRenderer(layoutId, object : DefaultItemViewRenderer<D>() {
            override fun render(data: D, viewBinder: DefaultViewBinder, payloads: MutableList<Any>) {
                renderer.invoke(data, viewBinder, payloads)
            }
        })
        return this
    }

    inline fun <reified D, VB : ViewBinder> addCustomRenderer(
            layoutId: Int,
            renderer: ItemViewRenderer<D, VB>): Adept {
        if (layouts.containsKey(D::class)) {
            throw IllegalArgumentException("Renderer for ${D::class} already registered")
        }
        layouts[D::class] = layoutId
        renderers[D::class] = renderer
        return this
    }

    inline fun <reified D, VB : ViewBinder> addCustomRenderer(
            layoutId: Int,
            crossinline renderer: (data: D, viewBinder: VB, payloads: MutableList<Any>) -> Unit): Adept {
        addCustomRenderer(layoutId, object : ItemViewRenderer<D, VB>() {
            override fun render(data: D, viewBinder: VB, payloads: MutableList<Any>) {
                renderer.invoke(data, viewBinder, payloads)
            }
        })
        return this
    }

    fun attachTo(recyclerView: RecyclerView): Adept {
        recyclerView.adapter = this
        return this
    }

    fun useDiffUtil(diffUtilCallback: DiffUtilCallback = DefaultDiffUtilCallback): Adept {
        this.diffUtilCallback = diffUtilCallback
        return this
    }

    fun useEndlessScrolling(
            loadingLayoutResId: Int = -1,
            endlessScrollingCallback: EndlessScrollingCallback): Adept {
        this.loadingLayoutResId = loadingLayoutResId
        this.endlessScrollingListener = EndlessScrollingListener(endlessScrollingCallback)
        return this
    }

    fun useEndlessScrolling(
            loadingLayoutResId: Int = -1,
            endlessScrollingCallback: (currentPage: Int, totalItemsCount: Int) -> Boolean): Adept {
        useEndlessScrolling(loadingLayoutResId, object : EndlessScrollingCallback {
            override fun onLoadMore(currentPage: Int, totalItemsCount: Int): Boolean {
                return endlessScrollingCallback.invoke(currentPage, totalItemsCount)
            }
        })
        return this
    }

    inline fun <reified T : ViewBinder> useCustomViewBinder(
            crossinline binder: (holder: AdeptViewHolder) -> T): Adept {
        useCustomViewBinder(object : BinderSetter<T> {
            override fun getCustomViewBinder(holder: AdeptViewHolder): T {
                return binder.invoke(holder)
            }
        })
        return this
    }

    inline fun <reified VB : ViewBinder> useCustomViewBinder(binderSetter: BinderSetter<VB>): Adept {
        this.customBinder = binderSetter
        return this
    }

    fun saveNestedRvStates(): Adept {
        this.saveNestedRvStates = true
        return this
    }
}