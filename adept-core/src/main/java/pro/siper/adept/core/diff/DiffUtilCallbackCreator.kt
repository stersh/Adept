package pro.siper.adept.core.diff

import androidx.recyclerview.widget.DiffUtil

class DiffUtilCallbackCreator(private val oldDataset: List<Any?>,
                              private val newDataset: List<Any?>,
                              private val callback: DiffUtilCallback) : DiffUtil.Callback() {
    override fun getOldListSize() = oldDataset.size

    override fun getNewListSize() = newDataset.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val newItem = newDataset[newItemPosition]
        val oldItem = oldDataset[oldItemPosition]
        return callback.areItemsTheSame(oldItem, newItem)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val newItem = newDataset[newItemPosition]
        val oldItem = oldDataset[oldItemPosition]
        return callback.areContentsTheSame(oldItem, newItem)
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val newItem = newDataset[newItemPosition]
        val oldItem = oldDataset[oldItemPosition]
        return callback.getChangePayload(oldItem, newItem)
    }
}