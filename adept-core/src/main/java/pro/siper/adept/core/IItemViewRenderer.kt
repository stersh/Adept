package pro.siper.adept.core

interface IItemViewRenderer<D, VB> {
    fun render(data: D, viewBinder: VB, payloads: MutableList<Any>)
}
