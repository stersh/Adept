package pro.siper.adept.core.endless

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

// Code from this tutorial: https://github.com/codepath/android_guides/wiki/Endless-Scrolling-with-AdapterViews-and-RecyclerView
internal class EndlessScrollingListener(
        private val endlessScrollingCallback: EndlessScrollingCallback
) : RecyclerView.OnScrollListener() {
    var hasNextPage = true

    private var loading = false
    private var previousTotalItemCount = 0
    private var currentPage = 1
    private val visibleThreshold = 10
    private val startingPageIndex = 1

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val totalItemCount = recyclerView.adapter?.itemCount ?: return

        val layoutManager = (recyclerView.layoutManager as LinearLayoutManager)
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()
        val visibleItemCount = layoutManager.findLastVisibleItemPosition() - firstVisibleItem

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) this.loading = true
        }
        // If it's still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false
            previousTotalItemCount = totalItemCount
            currentPage++
        }

        // If it isn't currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        if (!loading && (firstVisibleItem + visibleItemCount + visibleThreshold) >= totalItemCount) {
            val hasNextPage = endlessScrollingCallback.onLoadMore(currentPage, totalItemCount)
            loading = hasNextPage
            if (!hasNextPage) {
                this.hasNextPage = hasNextPage
            }
        }
    }
}