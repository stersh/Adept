package pro.siper.adept.core.diff

object DefaultDiffUtilCallback : DiffUtilCallback {
    override fun areItemsTheSame(oldItem: Any?, newItem: Any?): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Any?, newItem: Any?) = true
}
